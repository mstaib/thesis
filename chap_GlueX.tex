\chapter{The GlueX Detector} \label{ch:detector}
GlueX is the first experiment to run in Hall D of the Continuous Electron Beam Accelerator Facility (CEBAF) located at Jefferson Lab in Newport News, Virginia. 
The strength of the GlueX detector is the capability to detect both charged and neutral particles with robust particle identification.
This capability coupled with a tagged photon beam allow for exclusive reconstruction with nearly hermetic coverage, which is ideal for partial wave analyses that will be used to explore the hybrid meson spectrum.
The GlueX detector can be roughly divided into four main components that are described in this chapter: beamline instrumentation, calorimetry, particle identification, and charged particle tracking.
A picture of the completed main spectrometer of the GlueX detector can be found in Figure \ref{fig:GlueXPicture}, and a schematic overview of the detector elements can be found in Figure \ref{fig:GlueXSchematic}.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\textwidth]{figures/GlueXPicture.jpg}
	\caption{Picture of the completed GlueX spectrometer taken in August, 2014.}
	\label{fig:GlueXPicture}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\textwidth]{figures/CanonicalGlueXIllustration.png}
	\caption{Illustration of the GlueX experiment.}
	\label{fig:GlueXSchematic}
\end{figure}


\section{Photon Beamline}
The GlueX detector in Hall D was constructed as part of the 12 GeV upgrade project that doubled the maximum energy of CEBAF. 
An overview of the 12 GeV upgrade project is shown in Figure \ref{fig:12GeVUpgrade}.
CEBAF is capable of parallel operations of up to four experimental halls. 
A detailed schematic of many of the critical beamline components can be found in Figure \ref{fig:beamline_detail}.
The beam to halls A,B, and C may be extracted after $1-5$ passes through the accelerator.
An additional half-pass through the machine extracts a $250$ MHz electron beam to Hall D.
An overview of the layout of the photon beamline showing the relative location of the tagger hall, collimator cave, and experimental hall is shown in Figure \ref{fig:beamline_tech_drawing}.

%-Maybe more details about the accelerator (injector->c100s)

\begin{figure}[tb!]
	\centering
	\includegraphics[width=8cm]{figures/12GeVUpgrade.png}
	\caption{Overview of $12$ GeV upgrade project of the CEBAF accelerator (not to scale).}
	\label{fig:12GeVUpgrade}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\hsize]{figures/mainmachine_beamline_dwg.pdf}
	\caption{Detailed illustration of beamline components of the CEBAF accelerator (not to scale). Reproduced from \cite{TCR}.}
	\label{fig:beamline_detail}
\end{figure}

\begin{figure}[tb!]
	\includegraphics[viewport=1001 991 3020 1700,clip,angle=0,width=0.98\linewidth]{figures/D000000000-4002_RevB.pdf}
	\caption{Schematic of the Hall D complex including the experimental hall and photon tagging facility. JLab Drawing number D000000000-4002\_RevB.}
	\label{fig:beamline_tech_drawing}
\end{figure}

Once it has reached the tagger hall, the electron beam passes through a $50 \; \mu$m diamond radiator.
Details of the diamond radiator fabrication and performance can be found in Reference \cite{BrendanThesis}.
By aligning the electron beam with the specific crystal lattice of the diamond, the bremsstrahlung photons emitted can be made to be partially linearly polarized \cite{CoherentBremRef}.
The direction of the linear polarization can be controlled by rotating the crystal. 
We refer to linear polarization parallel to the floor as PARA and perpendicular to the floor PERP.
This coherent component is predominantly along the beam direction at small angles ($< 25 \; \mu rad$) while the incoherent bremsstrahlung radiation has a broader angular distribution. 
The energy at which the coherent radiation peaks is also related to the orientation of the crystal relative to the electron beam. 
The produced photon beam then travels from the tagger hall into the collimator cave. 
There the beam passes through a $3.4$ mm or $5.0$ mm diameter tungsten collimator in order to select photons with small angles relative to the electron beam.
This enhances the coherent component of the beam entering the experimental hall as illustrated in Figure \ref{fig:Col_effect}.
The important goals of the beamline instrumentation are to measure the energy, polarization and flux of the incoming photon beam.
This is achieved using three detector systems: the photon tagger, triplet polarimeter, and pair spectrometer. 

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/tdr_collimcuts.pdf}
		\caption{Photon Flux Vs. $E_{\gamma}$}
		\label{fig:Col_spectrum}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/tdr_pol_spectrum.pdf}
		\caption{Linear polarization fraction Vs. $E_{\gamma}$}
		\label{fig:Col_fraction}
	\end{subfigure}
	\caption{Plots showing the effect of collimation on (a) the beam profile, and (b) the polarization fraction. These are simulated for a $15 \; \mu$m thick diamond radiator with a $1 \; \mu$A electron beam current.}
	\label{fig:Col_effect}
\end{figure}

\subsection{Photon Tagger}
Most of the beam electrons incident on the diamond radiator pass through without interacting and are bent by the 1.5 T dipole tagger magnet to the electron beam dump.
Electrons that do interact in the radiator and have lost more than $25\%$ of their energy are deflected into an array of scintillator counters that comprise the tagging system.
The tagging system is broken into two detectors.
The tagger microscope (TAGM) covers the range of energy from 8.1-9.1 GeV with energy resolution of 0.1\% and is designed to measure the coherent peak with high resolution.
The tagger hodoscope (TAGH) covers the rest of the range from 3-12 GeV. 
This detector is excluded from the analysis in this thesis since polarization in this energy region is small. 
A picture of the tagger array is found in Figure \ref{fig:TaggerPicture}.
The timing resolution of the TAGM measured relative to the accelerator RF signal during the Spring 2016 run period is shown in Figure \ref{fig:TAGMResolution}.
For more details on construction and performance of the TAGM, see \cite{AlexBarnesThesis}.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\hsize]{figures/TaggerPicture.jpg}
	\caption{Picture of the tagger detectors. The TAGM is located under the black light-tight shroud near the center of the image. The beam dump is to the right.}
	\label{fig:TaggerPicture}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/TAGMTimingRes.png}
	\caption{TAGM timing resolution for each of the readout channels. The dashed red line is the design resolution. Many of the installed fibers did not reach the design specification for the Spring 2016 run. They have since been replaced.}
	\label{fig:TAGMResolution}
\end{figure}

\subsection{Triplet Polarimeter}
\label{sec:TPOL}
An important experimental parameter is the polarization fraction of the photon beam.
One method of determining this value is by measuring ``triplet photoproduction'' on a nuclear target.
Triplet photoproduction is related to the more common $e^+e^-$ pair production off the nucleus of the atom. 
In pair production we have $\gamma Z \rightarrow Z e^+ e^-$. 
In the semi-classical approximation the massive nucleus $Z$ remains approximately at rest, and the $e^+e^-$ pair is produced in the direction of the incoming photon.
Triplet production differs in that the interaction of the photon occurs with one of the electrons of the atom. 
We then have $\gamma e^- \rightarrow e^- e^+ e^-$. 
Our approximation of the heavy target in pair production no longer holds and the recoil electron may be detected.
The angular distribution of the recoil electron encodes information about the polarization of the incident photon.
The cross section for triplet photoproduction is given as 
\begin{equation}
\sigma_t(\phi)=\sigma_0 [1-P\Sigma\cos(2\phi)]
\end{equation}
where $\sigma_0$ is the unpolarized cross section, $P$ is the degree of polarization of the photon beam, $\phi$ is the azimuthal angle of the recoil electron with respect to the polarization plane of the photon, and $\Sigma$ is the beam asymmetry for triplet photoproduction \cite{TPOLNIM}.
This is a QED process and therefore $\Sigma$ can be calculated through perturbative methods and is determined to be about 0.2.
For details of this calculation, see \cite{TPOLNIM}.
In order to extract our beam polarization, the task is to measure the cross section as a function of $\phi$.

The Triplet Polarimeter (TPOL) is designed for just such a measurement.
The triplet polarimeter is placed in the photon beamline downstream of the collimator, before the pair spectrometer. 
The entire device is operated under vacuum.
A $75 \; \mu$m beryllium foil is inserted into the photon beamline before a flat doughnut-shaped silicon strip detector (SSD).
The SSD is broken into 32 azimuthal segments and measures the angle and energy of the recoil electron.
This measurement is performed in tandem with the main experiment and is triggered by the coincident $e^+e^-$ pair in the Pair Spectrometer.
An image of the interior of the TPOL vacuum enclosure is shown in Figure \ref{fig:TPOL_pic}.
The measured azimuthal distributions of the electrons for two polarization directions in the Spring 2016 run period at $E_\gamma=8.95$ GeV are shown in Figure \ref{fig:TPOL_Angles}.
The polarization fractions as a function of energy extracted from fits to these distributions are shown in Figure \ref{fig:TPOL_result}. 

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/TPOLpicture.jpg}
	\caption{Picture of the TPOL detector inside the vacuum enclosure. The beam enters from the right, passes through the beryllium foil mounted on the L-shaped beam, and proceeds into the experimental hall. The SSD is mounted after the beryllium foil. Reproduced from \cite{TPOLNIM}.}
	\label{fig:TPOL_pic}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/TPOL_PARA.jpg}
		\caption{PARA orientation}
		\label{fig:TPOL_PARA}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/TPOL_PERP.jpg}
		\caption{PERP orientation}
		\label{fig:TPOL_PERP}
	\end{subfigure}
	\caption{Measured TPOL angular distribution at $E_{\gamma}=8.95$ GeV. The two plots are for (a) photon polarization direction parallel to the floor (PARA) and (b) photon polarization direction perpendicular to the floor (PERP). Here the angle $\phi$ is measured relative to the same reference direction in both (a) and (b) so that the $\tfrac{\pi}{2}$ shift in the polarization direction is evident. Reproduced from \cite{TPOLNIM}.}
	\label{fig:TPOL_Angles}
\end{figure}

\begin{figure}[tb!]
	\centering
	%\includegraphics[height=8cm]{figures/TPOL_Results.jpg}
	\includegraphics[height=8cm]{figures/Sp2016Polarization.pdf}
	\caption{Polarization fraction as a function of beam energy for the Spring 2016 run period. Reproduced from \cite{TPOLNIM}.}
	\label{fig:TPOL_result}
\end{figure}

The results for the polarization fraction presented here are used in the extraction of the spin density matrix elements in Chapter \ref{chap:SDME}.
The average polarization for accepted events in $\gamma p \rightarrow p \threebody$ are found in Table \ref{tab:pol_table}.
While the TPOL provides our best independent measurement of the beam polarization, 
there are additional measurements that we can use to cross-check this result. 
Comparisons to asymmetries in $\rho$ and $\pi^0$ production can be found in \cite{DuggerTPOLUpdate}. 
The results from the TPOL are consistent with the $\pi^0$ beam asymmetry, and also consistent with results in exclusive $\rho$ production.
Measurements based on fitting the energy spectrum of the photon beam by the method in \cite{KenCBSA} also yield consistent results with these measurements.
The TPOL measurement should, in principle, be the cleanest method for extracting the beam polarization, hence these results are used in the analysis presented in this thesis (for more discussion see Chapter \ref{chap:SDME}).
These values differ from those presented in Reference \cite{Pi0Paper} due to a systematic in the determination of the polarization that was discovered after publication. 
The measurements presented in the reference are still consistent with these new values to within the claimed systematic uncertainties.

\begin{table}[tb!]
	\centering
	\begin{tabular}{|c|c|c|c|}
	\hline
	Polarization Direction & Polarization Fraction & $\sigma_{pol}^{stat.}$ & $\sigma_{pol}^{syst.}$ \\
	\hline
	%PARA & 0.429 & 0.009 & 0.007 \\
	%PERP & 0.376 & 0.008 & 0.006 \\
	PARA & 0.387 & 0.008 & 0.006 \\
	PERP & 0.378 & 0.008 & 0.006 \\
	\hline
	\end{tabular}
	\caption{Energy weighted polarization fraction for PARA and PERP polarization orientations in the Spring 2016 run period. Systematic errors are assigned as in Reference \cite{Pi0Paper}.}
	\label{tab:pol_table}
\end{table}

\subsection{Pair Spectrometer}
The Pair Spectrometer (PS) is the last beamline component before the beam enters the main spectrometer.
The purpose of the PS is to measure the flux of the incoming photon beam as a function of energy and to serve as a trigger for the TPOL measurement.
This is achieved by measuring $e^+e^-$ pairs produced off the TPOL radiator.
The detector consists of a large dipole magnet that deflects the two particles in opposite directions based on their charge.
These are then detected in a set of scintillating detectors that determine the energy of each particle by measuring the amount of deflection experienced in the dipole.
The energy spectrum determined by the PS for one run in the Spring 2016 data set is shown in Figure \ref{fig:PSFlux}.
More information can be found in \cite{PSNIM}.

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/PSFlux.png}
	\caption{Photon beam flux as measured by the PS as a function of energy. The coherent peak at 9 GeV is clearly seen. Reproduced from \cite{Pi0Paper}.}
	\label{fig:PSFlux}
\end{figure}

\section{Liquid Hydrogen Target}
Once the photon beam reaches the main spectrometer, it is incident on a liquid hydrogen target. 
The target cell is 30 cm long and conically shaped.
It is tapered from upstream to downstream, with the diameter decreasing from 2.42 to 1.56 cm. 
The target nominally operates around $18^{\circ}$ K and 18 psia. 
It is surrounded by a Rohacell layer, followed by the Start Counter.
This assembly is inserted into the bore of the Central Drift Chamber.
An image of the target cell without the Rohacell layer is found in Figure \ref{fig:target_picture}.
More details on the target specifications may be found in \cite{TCR}.

\begin{figure}[tb!]
	\centering
	\includegraphics[height=4.5cm]{figures/target_picture.jpg}
	\caption{The liquid hydrogen target cell for the GlueX experiment. Beam enters from the right of the image.}
	\label{fig:target_picture}
\end{figure}

\section{Calorimetry}
We have now progressed to the details of the main spectrometer. 
A top-down schematic of the subsystems that comprise the baseline GlueX detector is shown in Figure \ref{fig:GlueXTopDown}. 
The calorimeters are the final detectors on the path of particles coming from the target. 
They are designed for the detection of final-state photons present in the majority of channels targeted for hybrid meson searches in the experiment. 
They have also served as a useful filter for leptonic final states through measurement of E/p.
The Barrel Calorimeter (BCAL) is the outermost detector in the bore of the GlueX solenoid covering polar angles from $11^{\circ}$ to $126^{\circ}$.
Particles missing the downstream end of the BCAL are met by the Forward Calorimeter (FCAL) covering polar angles down to $1^{\circ}$. 
Both of these detectors are described in the sections below.

\begin{figure}[tb!]
	\includegraphics[width=\columnwidth]{figures/GlueXTopDown.png}
	\caption{Schematic of the layout of the baseline GlueX detectors.}
	\label{fig:GlueXTopDown}
\end{figure}

\subsection{Barrel Calorimeter}
\label{sec:BCAL}

The BCAL is a hollow cylinder with an inner radius of 65 cm and an outer radius of 90 cm. 
It is constructed of a fused matrix of scintillating fibers and lead.
This dense amalgamation constitutes roughly 15 radiation lengths of material in the radial direction.
All of the lead adds up to 28 tons and 2,663 km of scintillating fiber.
Readout via silicon photomultipliers is attached to each end of the detector.
This allows measurement of the position along the length of the BCAL by the time difference of the signal arrival at each end.
The readout is segmented at roughly $2^{\circ}$ in $\phi$, and is also segmented in $r$ to allow 3D reconstruction of the shower profile.
An overview of the detector can be found in Figure \ref{fig:BCAL_Overview}.
The timing resolution for charged and neutral showers can be found in Figure \ref{fig:BCAL_Timing}. 
The mass resolution for the inclusive production of $\pi^0$ and $\eta$ meson production as a function of energy are found in Figure \ref{fig:BCAL_EnergyRes}.
Studies to determine the individual photon resolutions and detection efficiency are currently ongoing within the collaboration.
More details of the detector design and construction can be found in \cite{TCR}.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\hsize]{figures/bcal_overview.pdf}
	\caption{Overview of BCAL detector. (a) An isometric view of the detector. (b) Demonstration of the angular coverage relative to the target. (c) Layout of readout modules. (d) Overview of detector readout module showing grouping of individual SiPMs into readout layers. Reproduced from \cite{TCR}.}
	\label{fig:BCAL_Overview}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[height=6cm]{figures/BCAL_time_res_vs_E_data.pdf}
	\caption{BCAL timing resolution as a function of shower energy for charged and neutral particles.}
	\label{fig:BCAL_Timing}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.42\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/BCALPi0Resolution.png}
		\caption{}
		\label{fig:BCALPi0}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.58\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/BCALEtaResolution.png}
		\caption{}
		\label{fig:BCALEta}
	\end{subfigure}
	\caption{Energy resolution in the BCAL as a function of photon energy in (a) $\pi^0$ and (b) $\eta$ reconstruction. Pairs with nearly equal photon energy are selected in each bin.}
	\label{fig:BCAL_EnergyRes}
\end{figure}

\subsection{Forward Calorimeter}
\label{sec:FCAL}

The FCAL is made up of 2800 lead-glass blocks, each $4 \times 4 \times 45 \textnormal{ cm}^3$, stacked behind the Time-of-Flight detector in a circular pattern.
Electromagnetic showers in the lead glass produce Cherenkov light that is detected by a photomultiplier attached to the back of each block.
The amount of light produced in the block is roughly proportional to the energy deposited in the block, and therefore can be used to measure the energy of the shower.
The showers typically spread over multiple blocks in the FCAL. 
By combining information from these channels as a reconstructed shower, the energy, position, and arrival time of the shower can be determined.
A picture of the stacked FCAL before installation of the dark-room enclosure that surrounds the detector can be found in Figure \ref{fig:FCAL_stacked}.
The resolution of the detector for $\pi^0$ reconstruction can be found in Figure \ref{fig:FCALPi0Resolution}.
More details of the detector design and construction can be found in \cite{TCR}.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=10cm]{figures/FCAL_stacked.jpg}
	\caption{Picture of the FCAL detector before the dark room was constructed showing the individual lead-glass blocks.}
	\label{fig:FCAL_stacked}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\hsize]{figures/FCALPi0Resolution.png}
	\caption{Reconstructed $\pi^0$ mass in the FCAL as a function of photon energy (left). Width of reconstructed $\pi^0$ peak versus energy (right). Reproduced from \cite{AdeshFCAL}. }
	\label{fig:FCALPi0Resolution}
\end{figure}

\section{Charged Particle Tracking}
One of the primary capabilities of the GlueX detector is nearly hermetic charged particle tracking. 
This is achieved by way of two detectors, the Central Drift Chamber (CDC) and the Forward Drift Chamber (FDC). 
The layout of the tracking detectors is shown in Figure \ref{fig:GlueXTopDown}.
The CDC is a straw tube chamber covering polar angles between $6^{\circ}$ and $168^{\circ}$, with optimal coverage between $29^{\circ}$ and $132^{\circ}$.
The FDC is a cathode strip chamber covering polar angles below $20^{\circ}$.
The details of each of these detectors are given below.

\subsection{Central Drift Chamber}
\label{sec:CDC}
The straw tubes of the CDC are each $0.775$ cm in radius and 150 cm long. 
A $20 \; \mu$m diameter wire runs down the center of each tube. 
The wires are held under tension in order to minimize displacements due to gravitational effects.
The straw tubes are fixed at their ends by the crimp pin assembly for the wires at each end of the tube.
The straws are glued to their neighbors within their ring at three points along the length of the detector. 
This should provide enough structural rigidity to the assembly to fix the geometry of the wire relative to the straw tube (more on this later in Section \ref{sec:CDC_Deformations}).

This wire is held at a voltage of +2125 V relative to the tube surrounding it.
The volume is filled with a 50:50 mixture of Argon and $\textnormal{CO}_2$.
The gas mixture was chosen to optimize position resolution in the detector \cite{CDCNIM}.
When a charged particle passes through the straw, it ionizes the gas, and the freed electrons drift towards the wire.
The high field gradient near the wire causes amplification of the initial seed electrons and this charge can be measured by the readout electronics.

The CDC consists of 28 layers of closely-packed straw tubes broken into 7 ``super-layers''. 
Three of the super-layers (1,4,and 7) are oriented parallel to the beam axis. 
The remaining super-layers are alternately oriented at $\pm 6^{\circ}$ relative to the beam axis. 
A picture showing a set of alternating ``stereo'' layers is found in Figure \ref{fig:CDCStereoPicture}.
This slight skew allows position determination of track positions along the direction of the beam.
In total, the CDC is comprised of 3522 wire/straw-tube assemblies.

The CDC measures the shortest drift time for ionization clusters to reach the wire.
The distance of closest approach of the track to the wire is well approximated by converting measurements of the drift time of hits in each straw to distance.
This drift radius defines a cylinder tangent to which the particle is known to have passed through the detector. 
When we fit tracks in the CDC, we are actually fitting a track to the tangents of cylinders assuming some model for propagation of the track.

The performance of the CDC after calibration is shown in Figure \ref{fig:CDCEffAndRes}.
The hit efficiency is very high, and begins to degrade somewhat near the edge of the straw.
The spatial resolutions also have exceeded the design resolution of $150 \; \mu$m.
The poor resolution at low drift times is a characteristic of the gas mixture used and is related to the high drift velocity near the wire.
The shape of this distribution is modeled in the tracking when assigning errors to the individual hits. 

The CDC also provides a measurement of the energy deposition along the length of the track.
The amount of energy deposited per unit length is a property of the momentum and the particle type.
At low momentum, this can be used to distinguish between protons and mesons up to roughly 1 GeV.
A plot showing the energy deposition per unit length for one run in the Spring 2016 period is shown in Figure \ref{fig:CDCdEdx}.
Protons can be clearly be identified in the positively-charged sample.

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/cdc_stereo_bw.jpg}
	\caption{Picture showing two opposite stereo layers during construction of the CDC.}
	\label{fig:CDCStereoPicture}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/CDCEfficiency.png}
		\caption{}
		\label{fig:CDCEfficiency}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/CDCResolution.png}
		\caption{}
		\label{fig:CDCResolution}
	\end{subfigure}
	\caption{(a) CDC hit-level efficiency as a function of distance from the wire. (b) Width of biased residual distribution for cosmic tracks in the CDC.}
	\label{fig:CDCEffAndRes}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/CDCdEdx_negative.png}
		\caption{}
		\label{fig:CDCdEdx_negative}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/CDCdEdx_positive.png}
		\caption{}
		\label{fig:CDCdEdx_positive}
	\end{subfigure}
	\caption{Energy deposition per unit length in the CDC for (a) negative and (b) positive charged particles. The proton band can be clearly seen.}
	\label{fig:CDCdEdx}
\end{figure}

\subsection{Forward Drift Chamber}
\label{sec:FDC}
The FDC is divided into four packages of six cells each for a total of 24 layers.
Each layer is rotated by $60^{\circ}$ relative to its neighbor.
In each layer, a plane of wires is held between two grounded cathode planes.
The wires alternate between field and sense wires with a pitch of 5 mm. 
The sense wires are held at +2200 V and are connected to readout electronics that record the arrival time for signals caused by ionization of the gas by charged particles.
The field wires are set at negative voltage to improve the circular symmetry of the field surrounding the sense wires.
The entire detector is filled with a 40:60 mixture of Argon and $\textnormal{CO}_2$.

What distinguishes this detector from a standard drift chamber is the addition of cathode planes on either side of the wire plane. 
These cathode planes are comprised of copper strips with a pitch of 5 mm on a Kapton substrate. 
They are instrumented to detect the induced charge on a set of these strips.
The pulse height spectrum across the strips allows determination of the avalanche position near the wires.
The cathode strips are oriented at $75^{\circ}$ and $105^{\circ}$ relative to the wire direction.
The charge recorded on the cathode strips is used to determine the position on each cathode plane. 
This information can then be combined to determine the position of the avalanche along the wire direction.
The position of the avalanche in the direction perpendicular to the wire is determined with lower precision, and is useful in aligning the cathode planes to the wires (see Section \ref{sec:FDCInternalAlignment}).
A pictorial representation of the general design of an FDC package is shown in Figure \ref{fig:FDCDetectionConcepts}.
The redundancy of three measurements to determine a 2D point in the detector helps to reject spurious noise hits.
Information from the wires and the cathodes are both used in the tracking of charged particles.

A picture of the detector showing the four packages can be found in Figure \ref{fig:FDCPicture}.
Each pair of cathode strips provides a spatial resolution better than $200 \; \mu$m in the direction along the wire.
Measurements of the drift time to the sense wires provides similar resolution in the direction perpendicular to the wires with high efficiency.
More details of the calibrations of the FDC can be found in Section \ref{sec:FDCInternalAlignment}.
Details of the performance of the detector can be found in \cite{FDCNIM}.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=\hsize]{figures/FDCDetectionConcepts.png}
	\caption{Illustration of the principle behind the FDC readout. The cathode strips are on opposite sides of the wire plane and are angled at $75^{\circ}$ and $105^{\circ}$ relative to the wire direction.}
	\label{fig:FDCDetectionConcepts}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/FDCPhoto.jpg}
	\caption{Picture of the completed FDC detector before installation into the solenoid.}
	\label{fig:FDCPicture}
\end{figure}

\section{Particle Identification Detectors}
While many of the detector systems in the experiment contribute to the identification of the reconstructed particle type, two detectors are expressly built for this purpose.
These are the Start Counter (SC) and the Time-of-Flight (TOF) detectors.
The SC immediately surrounds the target and records the start time for charged tracks.
The TOF is located downstream of the solenoid and provides a timing measurement for forward-going particles.
Both of these detectors are made of plastic scintillator that detects the passage of charged particles by producing light.
A brief description of the design and performance of these detectors is provided below.

\subsection{Start Counter}
\label{sec:SC}

The SC is primarily used to select the RF bunch associated with a particular event (for more details see Section \ref{sec:RFSelection}).
It is also used as a reference time for the time-to-distance conversion in the tracking detectors.
The detector is made of segmented plastic scintillator that is bent to taper around the target cell.
A CAD drawing of the SC assembly can be found in Figure \ref{fig:SCAssembly}.
The detector has a timing resolution of roughly 300 ps after correcting for the position of the track, exceeding the design goal of 350 ps.
A plot showing the timing performance of the individual sectors can be found in Figure \ref{fig:SCResolution}.
For more details on construction and performance of the SC, see \cite{EricPooserThesis}.

\begin{figure}[tb!]
	\centering
	\includegraphics[height=5cm]{figures/SCAssembly.png}
	\caption{CAD drawing of SC assembly. Reproduced from \cite{TCR}.}
	\label{fig:SCAssembly}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/SCResolution.png}
	\caption{Timing resolution for each of the individual SC sectors. Reproduced from \cite{EricPooserThesis}.}
	\label{fig:SCResolution}
\end{figure}

\subsection{Time of Flight}
\label{sec:TOF}
The Time of Flight detector (TOF) is a large wall of scintillating bars placed at the exit of the solenoid shown in Figure \ref{fig:TOFPicture}. 
It is designed to measure the flight time of charged particles exiting the solenoid after passing through the FDC packages.
With the exception of bars closest to the beamline, each bar is instrumented on both ends allowing reconstruction of the position along the bar.
By combining the path length of the particle found using the tracking with the timing information from the TOF, we can compare the expected flight time for a given particle species against the observed time-of-flight to reject misidentified particle hypotheses.
A plot of the $\beta$ versus p distribution is shown in Figure \ref{fig:TOFBeta}.
The TOF provides reasonable $\pi$/$K$ separation for particle momenta up to 2 GeV, and $\pi$/p separation up to 4 GeV.
More information on the timing characteristics of the TOF paddles may be found in Reference \cite{TOFNIM}.

\begin{figure}
	\centering
	\includegraphics[height=9cm,trim={0 10cm 0 6cm},clip]{figures/TOFPicture.jpg}
	\caption{The TOF detector mounted on its support frame.}
	\label{fig:TOFPicture}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/TOFBeta_negative.pdf}
		\caption{}
		\label{fig:TOFBetaNegative}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/TOFBeta_positive.pdf}
		\caption{}
		\label{fig:TOFBetaPositive}
	\end{subfigure}
	\caption{Relativistic velocity $\beta$ versus particle momentum for (a) negative and (b) positive charged tracks. Bands corresponding to electrons, pions, kaons, and protons are clearly seen. The band at $\beta \approx 0.8$ comes from misidentified RF times described in more detail in Section \ref{sec:RFSelection}.}
	\label{fig:TOFBeta}
\end{figure}

 \section{Spring 2016 Physics Trigger}
 \label{sec:Trigger}
As the Spring 2016 run was technically an engineering run for the experiment, the exact configuration of the experimental trigger was continuously changing. 
Conditions of the trigger thresholds were stable through the set of runs presented in this thesis, but were later adjusted to optimize performance.
The main experimental triggers rely on energy deposits in the BCAL and FCAL and are specified in terms of integrated ADC units. 
An approximate conversion from integrated ADC counts to energy is used in Table \ref{tab:trig_conditions} describing the trigger settings used to collect the data in this thesis. 
Due to high rates, inner layers of the FCAL near the beamline are masked in some of the triggers. 
The number of masked layers is indicated in the table.

For $\omega$ photoproduction, it is expected that these triggers should be fairly efficient since the dominant decays contain photons that deposit large amounts of energy in the calorimeters.
In the context of the overall GlueX physics program, most of the expected final states to be studied contain final state photons, so a trigger relying on neutral energy deposition is a natural choice \cite{TDR}. 
Currently the trigger is not implemented in the detector simulation,
and no cut is placed to select a single physics trigger type in the analysis.
This could lead to additional systematic uncertainties in our acceptance correction that are not explored in this thesis.
Further development is currently ongoing within the collaboration to address this issue.

\begin{table}
	\centering
	{\tabulinesep=1.0mm
		%\begin{tabu}{|c|c|c|}	
		\begin{tabu}{ | X[2.5,c,m] |X[0.75,c,m] | X[1.5,c,m] |}
			\hline
			Trigger Threshold & Masked FCAL Layers & Notes\\
			\hline
			\hline
			$E_{FCAL}+ 2.5 E_{BCAL} > 0.50 \gev$ and $E_{FCAL} > 0.05 \gev$& 4 & Main production trigger\\
			\hline
			$E_{FCAL}+ 0.6 E_{BCAL} > 0.50 \gev$    & 2& \\
			\hline
			$E_{BCAL} > 0.55 \gev$ &  0 & \\
			\hline
			$E_{FCAL} > 0.25 \gev$ and any SC hit & 2 & \\		
			\hline
		\end{tabu}
	}
	\caption{GlueX trigger settings for the Spring 2016 production run period. Threshold values are approximate. }
	\label{tab:trig_conditions}
\end{table}

% Triggers in ADC Units
%\begin{table}
%	\centering
%	{\tabulinesep=1.0mm
%		\begin{tabu}{|c|c|c|}	
%			\hline
%			Trigger Threshold [ADC counts] & Masked FCAL Layers & Notes\\
%			\hline
%			\hline
%			$10E_{Raw}^{FCAL} + 2E_{Raw}^{BCAL} > 18000$ and $E_{Raw}^{FCAL} > 200 $ & 4 & Main production trigger\\
%			\hline
%			$20E_{Raw}^{FCAL} + E_{Raw}^{BCAL} > 36000$ and $E_{Raw}^{FCAL} > 200 $  & 2& \\
%			\hline
%			$E_{Raw}^{BCAL} > 25000$ &  0 & \\
%			\hline
%			$E_{Raw}^{FCAL} > 900$ and any SC hit & 2 & \\		
%			\hline
%		\end{tabu}
%	}
%	\caption{GlueX trigger settings for the Spring 2016 golden run period.}
%	\label{tab:trig_conditions}
%\end{table}