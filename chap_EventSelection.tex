\chapter{Event Selection} \label{ch:eventselection}

\section{Data Selection}
In this chapter we describe the selection of data that will be used in Chapter \ref{chap:SDME} to extract the spin density matrix elements in $\omega$ photoproduction. 
The data were collected from April 14-20, 2016 at the tail end of the final GlueX engineering run period.
While much of this run period was spent studying hardware performance and tweaking the experimental parameters, the set of runs from 11366 to 11555 was a period of stable  ``production'' running during which minimal changes to the experiment were made.
This set of runs has come to be known colloquially as the \textit{golden} run period. 
A summary of the event statistics in the golden run period is presented in Table \ref{tab:run_statistics_2016}.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		Radiator & Polarization Orientation & Number of Triggers & Average $e^{-}$ Beam Current \\
		\hline
		\hline
		$50\; \mu \textnormal{m}$ diamond & PARA & 2,680,858,358 & 128 nA\\
		\hline
		$50\; \mu \textnormal{m}$ diamond & PERP & 2,878,849,718 & 138 nA\\
		\hline
		$3\times10^{-5} \textnormal{ RL Al}$ & Amorphous & 825,150,336 & 129 nA\\
		\hline
	\end{tabular}
	\caption{Summary of total statistics and run conditions in the Spring 2016 golden run period.}
	\label{tab:run_statistics_2016}
\end{table}

During this set of runs, the polarized photon beam was produced on a $50 \mu$m thick diamond radiator and passed through the $3.4$ mm collimator. 
The diamond was rotated between two perpendicular orientations, PARA where the polarization vector is parallel to the floor, and PERP where the polarization vector is perpendicular to the floor.
These two orientations are considered as separate independent data sets through most of the analysis. 
The results are only recombined to get our final result and to produce the plots in this chapter.

The electron beam was delivered at 250 MHz from the accelerator, corresponding to a bunch spacing of roughly 4 ns.
The GlueX solenoid was set to a current of $1200$ A which is slightly lower than nominal operation in subsequent run periods.
This slightly impacts the momentum resolution for charged tracks, but simulations indicate much of the resolution that is lost can be recovered for exclusive reconstruction by kinematically fitting the event \cite{SolenoidStudies}.

During the golden run period, the nominal beam current had to be set slightly higher for PERP (138 nA) than PARA (128 nA) configurations in order to match DAQ rates in the hall between the two settings. 
This has been attributed to possible misalignment of the photon beam spot on the collimator.
Efforts to understand the impact of the backgrounds caused by this increased beam current are still ongoing.
For this analysis, it is assumed that the subtraction of tagger accidentals described in section \ref{sec:RFSelection} removes the dependence of the results on the beam current.

\section{Data Processing}
There are several stages in our analysis chain during which cuts are applied to the data to extract our signal. 
After calibration, the raw data is processed into a compressed XML data format for Reconstructed Event STorage (REST). 
Reconstructed momentum/energy four-vectors, positions of the tracks and showers and their covariance matrices are stored. 
All hit-level information is discarded.
When improvements to the low-level calibrations or reconstruction are made, these REST files must be reproduced from the raw data.
The analysis presented in this thesis was performed with the version 4 reconstruction of the Spring 2016 data.
This REST production includes the improvements made to the tracking detector alignment and calibrations described in chapter \ref{ch:tracking}.

\section{Assembly of Particle Combinations}
\label{sec:particle_combo}
Starting from the REST data, we can form combinations of tracks and showers consistent with our desired final states.
For the hadronic decay, combinations consistent with two positively charged tracks, one negatively charged track and two neutral showers are assembled from the reconstructed data. 
Both mass hypotheses are tested for the proton and $\pi^{+}$ in the case of the positive tracks ($p_1\pi_2^+$ and $\pi_1^+ p_2$).
In the case of the radiative decay, one positively charged track assigned to the mass of the proton and three neutral showers are required.
Up to four additional ``good'' charged tracks besides those used in the combination are permitted in the event.
These ``good'' tracks are simply those that have a matching hit in one of the detectors besides the tracking chambers. 
There are no other cuts on the track quality.
We also require that there are no extra neutral showers in the calorimeters. 
The significance of this cut is currently being investigated.
Results may change as improvements are made to charged-track clustering in the BCAL.

\subsection{Beam Photon Selection}
\label{sec:RFSelection}
As part of the track-fitting routines, charged tracks are matched to hits in the SC, TOF, BCAL, and FCAL.
Using the path-length of the track, the time of the matched hit is propagated to the point of closest approach to the beamline. 
This is the ``vertex time''.
In the GlueX experiment, the reference plane for timing is chosen to be at the center of the liquid-hydrogen target.
A correction is made to the vertex time to account for the distance between the vertex location and our reference plane.
We will refer to the time at the center of the target as the particle's ``target time'', $t_{Target}$.
In order to match the combination to a tagged beam photon, each of the charged particles in the combination vote by selecting the nearest 4 ns beam bucket to each $t_{Target}$ for every matched hit.
Photons detected in the calorimeters are given a single vote. 
Photon flight times are corrected based on the position of the photon shower.
Each vote is given equal weight.
Ties are then broken by the summed squared residual of each target time to the RF signal coming from the accelerator.
The ``RF Time'' is then reported as the incoming beam photon time at the center of the target for this selected beam bucket.

The selected RF time is then compared to the time of the reconstructed tagged photons. 
The tagger is calibrated such that each of the individual channels are aligned to the same RF signal.
A cut is placed on the difference between the measured time in the tagger and the selected RF time in the main spectrometer.
In most cases, the correct beam photon associated with the event should appear centered near $\Delta t = 0$ if our selection procedure is successful.
A plot of the difference between the tagged photon time and the RF time selected by the main spectrometer is shown in figure \ref{fig:RFSelection}.
In addition to this main peak, there are clearly additional bunches to the side spaced at the period of the beam bunches.
These ``accidental tags'' are typically caused by real electron hits in other tagger channels near the photon energy that caused the trigger.
Occasionally, the accidental hit will happen to arrive at the same time as the correct photon in a nearby counter.
The structure seen in the sidebands is therefore presumably lurking under the main peak.
We can correct for this ``accidental background'' by subtracting the contribution from the sidebands in our analysis.
It will be indicated as ``accidentals-subtracted'' in the caption of figures to follow when this has been done.

It is clear from figure \ref{fig:RFSelection} that the individual beam bunches are not resolved perfectly. 
The width of these distributions is driven almost entirely by the timing resolution of the tagger microscope.
In the Spring 2016 run period, there were many individual fibers that did not meet the original design specification.
These fibers had low light yield causing poor timing resolution.
This poor resolution causes some of the spill over from the main peak into the adjacent bunches.
We have chosen to cut these directly adjacent bunches from the analysis.
Most of these low-yield fibers were replaced before the Spring 2017 run period.

\begin{figure}
	\centering
	\includegraphics[width=9cm]{figures/RFSelection.pdf}
	\caption{Difference between hit time in the tagger and the selected RF bunch in the main spectrometer. The sideband bunches (blue) are used to subtract the contribution of tagger accidentals from the main peak (green).}
	\label{fig:RFSelection}
\end{figure}

\subsection{Kinematic Fitting}
\label{sec:kinematic_fit}
A kinematic fit attempts to optimize measured particle parameters by constraining the results to match a hypothesis about the underlying physics process.
In this analysis, the particle combination along with its beam photon are subjected to a kinematic fit to the hypothesis of conserved four-momentum. 
In the hadronic decay, $\omega\rightarrow\threebody$, the hypothesis of a common vertex is also included in the fit.
In the kinematic fit, the mass of the $\pi^{0}$ originating from the decay of the $\omega$ is constrained to the PDG value in the fit as the width is negligible. 
However, the mass of the $\omega$ meson is not fixed, as it has non-negligible width.
We define $\bm{\eta}$ as the estimated observable parameters (momenta and position), $\mathbf{y}$ as the measured observable parameters, and $\mathbf{V}_{\mathbf{y}}$ as the covariance matrix of measured data.
The goal of the kinematic fit is to minimize 
\begin{align}
\chi^2=(\mathbf{y} - \bm{\eta})^T \mathbf{V}_{\mathbf{y}}^{-1} (\mathbf{y} - \bm{\eta})
\end{align}
subject to the constraints imposed by our hypothesis (usually implemented as Lagrange multipliers).
The result of the fit is a set of improved measurements $\bm{\eta}$ and their covariance matrix $\mathbf{V}_{\bm{\eta}}$.
Additional details of the kinematic fitter for the GlueX experiment can be found in Paul Mattione's magnum opus \cite{PaulKinFit}.
The improved four-vectors and vertex positions that result from the kinematic fit are used in the remainder of the analysis.

Kinematic fitting is sensitive to the accuracy of the parameters and error matrices returned by the particle reconstruction that serve as input to the routines.
To test the validity of this fit given the early stage of reconstruction validation, the distribution of the ``confidence level'' (CL) of the kinematic fit can be inspected.
Assuming the weighted residuals of the measured kinematics to our hypothesis are distributed as the standard normal ($\mu=0, \sigma=1$), we may define the probability that a random $\chi^2$ will exceed the measured value, i.e.
\begin{equation}
P(\chi^2, NDF) = 1 - CDF(\chi^2,NDF)
\end{equation}
where $CDF$ is the cumulative distribution function of a $\chi^2$ statistic with $NDF$ degrees of freedom.
This probability is referred to as the confidence level of the fit.
If our assumptions about the distributions of the weighted residuals hold, this value should be uniformly distributed from 0 to 1.
Thus by inspecting the shape of this distribution, we can make some rough statements about the quality of the fit itself.

Plots of the kinematic fit confidence level for all combinations passing the loose selection cuts in section \ref{sec:particle_combo} are shown in figure \ref{fig:MassVskinfitCL}.
In this figure, we have plotted the invariant mass of our candidate $\omega$ against the log of the kinematic fit confidence level. 
Our signal combinations are clearly clustered at high CL.
In the \threebody decay plot, there is indication of exclusive $\eta(549)$, $\omega(782)$ and $\phi(1020)$ production clearly separated from the background at high CL.
Since $\omega$ is the only particle with a sizable branch to $\pi^0 \gamma$, it is the only resonance that shows up at high CL in figure \ref{fig:MassVskinfitCL_radiative}.
Combinations with a confidence level less than $0.01$ are cut in the analysis that follows.
 
After applying additional cuts described in subsequent sections, we arrive at the distribution of the kinematic fit confidence level found in figure \ref{fig:KinFitCL}.
Typically there is pileup at low confidence level due to deviations from Gaussian experimental errors.
%One example could be the multiple scattering modeled in the fit of the charged tracks is only approximately normal, so the ideal assumptions above are not satisfied.
At high confidence level, these distributions should be flat if the errors reported for the particles are accurate.
In the hadronic decay this seems to be the case. 
In the radiative decay there is a downward slope that may indicate these errors are being underestimated in the kinematic fit.
An additional useful quantity to inspect are the pulls of the fits, defined for each measurement as 
\begin{align*}
z_i &\equiv \frac{y_i - \eta_i}{\sigma(y_i - \eta_i)}
\end{align*}
with 
\begin{align*}
\sigma^2(y_i - \eta_i) &= (\mathbf{V}_{\mathbf{y}})_{ii} - 2 \textnormal{cov}(\mathbf{y},\bm{\eta})_{ii} + (\mathbf{V}_{\bm{\eta}})_{ii} \\
 &= (\mathbf{V}_{\mathbf{y}})_{ii}  - (\mathbf{V}_{\bm{\eta}})_{ii}
\end{align*}
As an example, distributions of the kinematic-fit pulls for the individual components of momenta and position of the recoil proton in the reaction $ \gamma p \rightarrow p \pi^+ \pi^- \pi^0$ are shown in Figure \ref{fig:KinFitPulls}.
These are fit with a Gaussian, and the corresponding mean ($\mu$) and standard deviation ($\sigma$) are extracted. 
Tables \ref{tab:HadronicKinFitPulls} and \ref{tab:RadiativeKinFitPulls} contain the pull means and widths for each of the reconstructed particles used as input to the kinematic fits.
These results still show some small biases ($\mu \neq 0$) and improper modeling of the error ($\sigma \neq 1$), particularly in the neutral reconstruction.
Detailed studies are still ongoing to validate our understanding of the experimental errors.
These discrepancies from ideal behavior can cause a small systematic uncertainty associated with the kinematic fit results presented in this thesis.
The systematic uncertainty associated with the cut value on the kinematic-fit confidence level is investigated in section \ref{sec:systematic_uncertainties}.

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/M3Pi_Vs_KinFitCL.png}
		\caption{$\gamma p \rightarrow p \threebody$}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MPiGamma_Vs_KinFitCL.png}
		\caption{$\gamma p \rightarrow p \twobody$}
		\label{fig:MassVskinfitCL_radiative}
	\end{subfigure}
	\caption{\threebody and \twobody invariant mass as a function of kinematic fit confidence level.}
	\label{fig:MassVskinfitCL}
\end{figure}

\begin{figure}[tb!]
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/KinFitCL_hadronic.png}
		\caption{$\gamma p \rightarrow p \threebody$}
		\label{fig:KinFitCL_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/KinFitCL_radiative.png}
		\caption{$\gamma p \rightarrow p \twobody$}
		\label{fig:KinFitCL_radiative}
	\end{subfigure}
	\caption{Accidental-subtracted kinematic fit confidence level for all particle combinations in the golden run period passing all selection cuts presented in this chapter.}
	\label{fig:KinFitCL}
\end{figure}

\begin{figure}[tb!]
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Px.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Py.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Pz.pdf}
	\end{subfigure}%
	\\
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Xx.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Xy.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/Pull_Xz.pdf}
	\end{subfigure}
	\caption{Pull distributions for the reconstructed proton in a kinematic fit to $\gamma p \rightarrow p \threebody$ requiring four-momentum conservation and a common vertex location. Fits with a confidence level lower that 0.025 are excluded from these plots. The $\pi^0$ mass is constrained in the fit.}
	\label{fig:KinFitPulls}
\end{figure}

\begin{table}
	\centering
	\begin{tabular}{c|cccccc}
		%proton
		Proton Pulls & $p_x$ & $p_y$ & $p_z$ & $x$ & $y$ & $z$ \\
		\hline
		$\mu$ & -0.131& 0.030& -0.047 & 0.004 & -0.067 & -0.055 \\
		$\sigma$ & 1.167 & 1.160 & 1.234 & 1.039 & 1.036 & 1.057 \\
		%pi+
		$\pi^+$ Pulls & $p_x$ & $p_y$ & $p_z$ & $x$ & $y$ & $z$ \\
		\hline
		$\mu$ & -0.015 & -0.072 & 0.019 & 0.033 & 0.087 & 0.053 \\
		$\sigma$ &1.107 & 1.084 & 1.297 & 1.039 & 1.031 & 0.996\\
		%pi-
		$\pi^-$ Pulls & $p_x$ & $p_y$ & $p_z$ & $x$ & $y$ & $z$ \\
		\hline
		$\mu$ & -0.149 & 0.016 & 0.120 & 0.060 & 0.017 & 0.062 \\
		$\sigma$ &1.139& 1.125 & 1.328 & 1.048 & 1.045 & 1.034 \\
		%gamma
		$\gamma$ Pulls & \multicolumn{3}{c}{Energy} & $x$ & $y$ & $z$  \\
		\hline
		$\mu$ & \multicolumn{3}{c}{-0.005} & -0.263 & -0.033 & -0.010 \\
		$\sigma$ &\multicolumn{3}{c}{1.480} & 1.334 & 1.329 & 1.443 \\
	\end{tabular}
	\caption{Mean and width of Gaussian fits to the pull distributions for the kinematic fit to $\gamma p \rightarrow \threebody$ requiring four-momentum conservation and a common vertex location. The $\pi^0$ mass is constrained in the fit. } 
	\label{tab:HadronicKinFitPulls}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{c|ccc}
		%proton
		Proton Pulls & $p_x$ & $p_y$ & $p_z$ \\
		\hline
		$\mu$ & -0.121 & 0.033 & 0.053 \\
		$\sigma$ & 1.287 & 1.256 & 1.302 \\
		%pi+
		Prompt $\gamma$ Pulls & $p_x$ & $p_y$ & $p_z$  \\
		\hline
		$\mu$ & -0.090 & 0.015 & -0.078 \\
		$\sigma$ &1.330 & 1.304 & 1.436 \\
		%pi-
		$\gamma$ from $\pi^0$ Pulls & $p_x$ & $p_y$ & $p_z$  \\
		\hline
		$\mu$ & -0.098 & 0.015 & -0.107 \\
		$\sigma$ &1.360 & 1.361 & 1.440 \\
	\end{tabular}
	\caption{Mean and width of Gaussian fits to the pull distributions for the kinematic fit to $\gamma p \rightarrow \twobody$ requiring four-momentum conservation. The $\pi^0$ mass is constrained in the fit. } 
	\label{tab:RadiativeKinFitPulls}
\end{table}

\section{Additional Exclusivity Cuts}

\subsection{Measured Missing 4-Momenta}

To ensure that strong outliers are not passing our kinematic-fit confidence level cut due to improper errors, we inspect the measured kinematics that have not been improved by kinematic fitting.
The missing four-momentum of a given combination is
\begin{equation}
p_{Missing}=\sum_{i=1}^{N_{i}} p^{i}_{Measured} -\sum_{f=1}^{N_{f}} p^{f}_{Measured}
\end{equation}
where the index $i$ runs over the particles in the initial state, and $f$ those in the final state.
The missing mass squared of the combination is given by 
\begin{equation}
MM^{2} = E_{Missing}^{2}-\mathbf{p}_{Missing}^{2}.
\end{equation}

The accidentals-subtracted missing-mass-squared is plotted for the hadronic and radiative decay of the omega in Figure \ref{fig:MM2}.
This plot includes all other selection cuts placed on the data.
In both decays, the value is centered tightly around zero and there is very little background. 
In reality, our previous cut on the kinematic fit CL ensured this would be the case.
A very loose cut is placed at $\pm 0.05 \textnormal{ GeV}^2$ to reject any spurious combinations that may be outliers.
We can also inspect the individual components of the missing four-momentum.
The accidentals-subtracted missing energy is shown in Figure \ref{fig:MissingE}.
A cut is placed at $\pm 1.0 \textnormal{ GeV}$ and is indicated on the plot. 
%In the radiative decay, the mean of this distribution is noticeably shifted from zero. 
%The cause of this small shift is not yet fully understood.
The accidentals-subtracted missing transverse momentum is shown in figure \ref{fig:MissingPt}.
A cut is placed at $0.25 \textnormal{ GeV}$ in the analysis that follows.

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MM2_hadronic.png}
		\caption{$\gamma p \rightarrow p \threebody$}
		\label{fig:MM2_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MM2_radiative.png}
		\caption{$\gamma p \rightarrow p \twobody$}
		\label{fig:MM2_radiative}
	\end{subfigure}
	\caption{Accidentals-subtracted measured missing-mass-squared. All other analysis cuts are applied.}
	\label{fig:MM2}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MissingE_hadronic.png}
		\caption{$\gamma p \rightarrow p \threebody$}
		\label{fig:MissingE_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MissingE_radiative.png}
		\caption{$\gamma p \rightarrow p \twobody$}
		\label{fig:MissingE_radiative}
	\end{subfigure}
	\caption{Accidentals-subtracted measured missing energy. All other analysis cuts are applied.}
	\label{fig:MissingE}
\end{figure}

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MissingPt_hadronic.png}
		\caption{$\gamma p \rightarrow p \threebody$}
		\label{fig:MissingPt_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MissingPt_radiative.png}
		\caption{$\gamma p \rightarrow p \twobody$}
		\label{fig:MissingPt_radiative}
	\end{subfigure}
	\caption{Accidentals-subtracted measured missing transverse momentum. All other analysis cuts are applied.}
	\label{fig:MissingPt}
\end{figure}
\FloatBarrier

\subsection{Measured $\pi^0$ Mass}
We may also inspect the measured $\pi^{0}$ invariant mass. 
The kinematic fit constrains the mass to the correct PDG value.
The measured invariant mass distributions are found in figure \ref{fig:Pi0Mass}.
The tails in this distribution are cut as indicated in the figure.
The measurement of the SDMEs in chapter \ref{chap:SDME} are not strongly influenced by the value of this cut. 

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MassPi0_hadronic.png}
		\caption{}
		\label{fig:Pi0Mass_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/MassPi0_radiative.png}
		\caption{}
		\label{fig:Pi0Mass_radiative}
	\end{subfigure}
	\caption{Accidentals-subtracted $\pi^{0}$ invariant mass for the (a) hadronic and (b) radiative decay.}
	\label{fig:Pi0Mass}
\end{figure}
\FloatBarrier
\subsection{Vertex Location}
Since we are interested in particle combinations originating in the hydrogen target, we place a cut on the vertex position returned by the kinematic fit.
Plots of the kinematic fit vertex position are found in figure \ref{fig:vertex} and the corresponding cut regions are indicated.
%The total width of the x-y profile of the beam is defined by the collimator.
%This is slightly offset from the origin of our coordinate system.
Since we require the recoil proton be detected in this exclusive measurement, there are very few events outside of the target in the $z$ projection.

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/vertexR_hadronic.png}
		\caption{}
		\label{fig:vertexR_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/vertexZ_hadronic.png}
		\caption{}
		\label{fig:vertexZ_hadronic}
	\end{subfigure}
	\caption{Kinematic fit vertex position (a) versus $xy$ (b) versus $z$. Selected regions are indicated by the red lines. }
	\label{fig:vertex}
\end{figure}

\subsection{Measured Photon Kinematics}
In the fit for the spin-density-matrix elements presented in Chapter \ref{chap:SDME} it is required that we make a correction for the experimental acceptance.
One critical component of this correction is correctly modeling the photon reconstruction.
At the time of the writing of this thesis, inefficient FCAL blocks have not been removed from the reconstruction at the hit level.
As an ad-hoc correction, we may mask certain regions of the detector based on the shower position.
A plot of the FCAL shower position for photons passing our selection cuts is found in figure \ref{fig:FCAL_Acceptance}. 
There are clear issues with the reconstruction near the beamline. This region is excluded by a circular cut around the deficient region.
The outer region of the detector is also cut as there is some overlap with the BCAL acceptance in this region that is poorly modeled.
Regions surrounding individual problematic channels in the accepted region have also been masked in the analysis.

The reconstructed photon position in the BCAL is plotted in figure \ref{fig:BCAL_Acceptance}. 
The very forward region is cut as low-angle-of-incidence showers require more study before inclusion in the analysis. 
This forward cut could be relaxed as validation of the Monte Carlo proceeds.
The cut near the middle of the detector is driven by low accepted Monte Carlo statistics below this z position.
This can be addressed through a larger-scale production of Monte Carlo data for the acceptance correction.
For this analysis, this is unnecessary since it is found that the value of this cut does not greatly impact our final measurement.
In the radiative decay, the BCAL is completely excluded from the analysis.
Including the BCAL region causes large uncertainties in the measured SDMEs. 
The cause of this effect is not yet understood.
The systematic uncertainty ascribed to this cut is studied in section \ref{sec:systematic_uncertainties}.
 
\begin{figure}[tb!]
	\centering
	\includegraphics[height=10cm]{figures/FCAL_hadronic.png}
	\caption{FCAL shower positions in the hadronic decay. The region between the two circles are selected for the analysis. Individual problematic channels in the accepted region have been excluded from the analysis.}
	\label{fig:FCAL_Acceptance}
\end{figure}
\begin{figure}[tb!]
	\centering
	\includegraphics[height=7cm]{figures/BCAL_hadronic.png}
	\caption{BCAL shower positions in the hadronic decay. The region between the two lines is selected for the analysis.}
	\label{fig:BCAL_Acceptance}
\end{figure}

\subsection{Particle Identification Based on dE/dx}
A useful tool for selecting between different mass-hypotheses for charged tracks is measurements of the energy loss along the track from interactions with the detector medium.
The CDC is well suited for this measurement as discussed earlier in section \ref{sec:CDC}. 
Typically a cut on this value is used to separate positively charged pions from protons at momenta less than $\~1 \textnormal{ GeV}$.
However, after applying the exclusivity cuts described in previous chapters, we find additional cuts based on this quantity to be unnecessary for the analysis.
The distributions of the energy deposition per unit length for positive particles are shown in figure \ref{fig:dEdx_CDC} as a function of the particle momentum.
There is little sign of misidentification of the two particle types.

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/dEdx_protons.png}
		\caption{}
		\label{fig:dEdx_CDC_proton}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/dEdx_piplus.png}
		\caption{}
		\label{fig:dEdx_CDC_pion}
	\end{subfigure}
	\caption{dE/dx versus measured particle momentum for (a) protons and (b) positively charged pions as identified by kinematic fit and missing-mass cuts. There is clear separation of the proton from the $\pi^{+}$.}
	\label{fig:dEdx_CDC}
\end{figure}

\subsection{Particle Identification Based on Time-of-Flight}
An additional method for discriminating between particle types is using the time-of-flight measurements from the target to the different timing detector elements.
%For charged particles, this is a superior method to cuts based on the relativistic velocity, $\beta$, since 
For charged particles, energy loss may be modeled along the track and factored into the expected hit time.
At the first analysis stage, there is a loose cut on each of the timing measurements of $\pm 4 \textnormal{ ns}$. 
After the exclusivity cuts described in this chapter, the difference of the tagged photon RF-time and the target-time of the particle in its primary timing detectors is plotted in figure \ref{fig:delta_t}.
The spurious events that are present outside of the main bands are not necessarily consistent with misidentification of the particle. 
These would appear as separate diverging bands instead of random noise.
For this analysis, no additional cut is placed on the timing. 
The choice to not place a cut is also motivated by the early stage of ongoing work to match timing resolutions between Monte Carlo and data.

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{\hsize}
		\centering
		\includegraphics[width=0.5\textwidth]{figures/deltaT_proton_BCAL.png}
		\caption{}
		\label{fig:delta_t_proton_BCAL}
	\end{subfigure}%
	\\
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/deltaT_piplus_TOF.png}
		\caption{}
		\label{fig:delta_t_piplus_TOF}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/deltaT_piminus_TOF.png}
		\caption{}
		\label{fig:delta_t_piminus_TOF}
	\end{subfigure}%
	\\
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/deltaT_gamma_BCAL.png}
		\caption{}
		\label{fig:delta_t_gamma_BCAL}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/deltaT_gamma_FCAL.png}
		\caption{}
		\label{fig:delta_t_gamma_FCAL}
	\end{subfigure}
	\caption{ $\Delta$TOF versus measured particle momentum for (a) proton candidates hitting the BCAL, (b) $\pi^+$ candidates hitting the TOF, (c)  $\pi^-$ candidates hitting the TOF, (d) photon candidates hitting the BCAL, and (e) photon candidates hitting the FCAL. There is no indication of misidentified background. No additional cut is applied on these values.}
	\label{fig:delta_t}
\end{figure}

\subsection{Summary of Analysis Cuts}
A summary of the analysis cuts placed on our candidate particle combinations can be found in table \ref{tab:cuts}. 
After applying these cuts the effective number of signal combinations surviving after accidental subtraction is 156,973 in the hadronic decay and 18,398 in the radiative decay.
The PARA and PERP data samples have been combined and no cuts on Mandelstam $t$ are applied for this estimate.

\begin{table}[tb!]
	\centering
	\begin{tabular}{ |c|c| }
		\hline 
		{Quantity} & {Nominal Cut Value}  \\
		\hline
		Kinematic Fit Confidence Level      &        $0.01$           \\
		Measured Missing Mass Squared  &        $[-0.05, 0.05] \; \textrm{GeV}^{2}$          \\
		Maximum Measured Missing $p_{t}$ &     $0.25  \; \textrm{GeV}$ \\
		Measured Missing Energy  &  $[-1.0, 1.0] \; \textrm{GeV}$ \\
		Photon Beam Energy &  $[8.4, 9.0] \; \textrm{GeV}$ \\
		Measured $\pi^{0}$ Invariant Mass & $[0.12, 0.15] \; \textrm{GeV}$ \\
		Kinematic-Fit $\omega$ Invariant Mass & $[0.76, 0.81] \; \textrm{GeV}$ \\
		Kinematic-Fit-Vertex Z & $[50.0, 77.0] \; \textrm{cm}$ \\
		Maximum Kinematic-Fit Vertex R & $1.0 \; \textrm{cm}$ \\
		BCAL Neutral Shower Z & $[200.0, 380.0] \; \textrm{cm} $ \\
		FCAL Neutral Shower R & $[20.0, 100.0] \; \textrm{cm}$ \\
		Kinematic-Fit Proton $\theta_{lab}$ & $[52.0^{\circ}, 78.0^{\circ}]$ \\
		Minimum Kinematic-Fit Pion $\theta_{lab}$ & $1.0^{\circ}$ \\
		\hline
	\end{tabular}
	\caption{Summary of analysis cuts performed to extract our exclusive $\omega$ signal.}
	\label{tab:cuts}
\end{table}

 \section{Combinations Passing Analysis Cuts}
 \subsection{$\omega$ Invariant Mass}
With all of the cuts applied to the data, we can plot the invariant mass of our candidate $\omega$ measured in decays to \threebody and \twobody. 
These distributions are shown in figures \ref{fig:M3pi} and \ref{fig:M3gamma}.
Measured experimental resonance lineshapes are typically well approximated by the convolution of a non-relativistic Breit-Wigner (Lorentzian) function that describes the resonance, and a Gaussian that describes detector resolution. 
this function is known as a Voigtian.
The invariant-mass distributions are fit with the sum of a Voightian profile and a linear background.
These fits indicate a sample of high purity in both decay modes.
The quoted purity is obviously dependent on the lineshape and background model.
In the hadronic decay, the fit does not seem to describe the peak or the right tail of the peak very well.
This will impact forthcoming measurements of the cross sections for this reaction, but should not affect the measurements presented in this thesis.
For the radiative decay, the lineshape describes the data quite well, albeit with a higher Lorentzian width than the PDG value (14.8 MeV vs. 8.5 MeV).
Aside from the analysis presented here, this high-purity sample will be useful for additional detailed studies of the detector performance ongoing within the collaboration.

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/M3Pi.pdf}
	\caption{Accidental-subtracted \threebody invariant mass for all combinations passing selection cuts. The peak is fit with a Voightian plus a linear background. The signal and background integrals are performed over the mass range of 0.76 to 0.81 GeV selected for analysis.}
	\label{fig:M3pi}
\end{figure}

\begin{figure}[tb!]
	\centering
	\includegraphics[height=8cm]{figures/M3gamma.pdf}
	\caption{Accidental-subtracted \twobody invariant mass for all combinations passing selection cuts. The peak is fit with a Voightian plus a linear background. The signal and background integrals are performed over the mass range of 0.76 to 0.81 GeV selected for analysis.}
	\label{fig:M3gamma}
\end{figure}

\subsection{$|t|$ Distribution}
For the 2-body to 2-body reaction $ 1+2 \rightarrow 3+4 $, The Mandelstam variable $t$ is defined as
\begin{equation}
t=|p_1-p_3|^2=|p_2-p_4|^2.
\end{equation}
We use the kinematic-fit proton four-momentum and the four-momentum of our target proton (at rest in the lab frame) to calculate $t$ for each combination.
The accidentals-subtracted $|t|$-distribution of the selected combinations is shown in \ref{fig:t}. 
This distribution has the exponential falloff that is characteristic of diffractive $t$-channel production at low $|t|$.
For the fits that follow in Chapter \ref{chap:SDME}, we have chosen to bin the data in four bins of $|t|$ for the hadronic decay ranging from $0.10$ to $0.80 \textnormal{ GeV}^2$. Due to lower statistics and large systematic uncertainties, the radiative decay is fit in a single bin of $|t|$ from $0.10$ to $0.60 \textnormal{ GeV}^2$. 

\begin{figure}[tb!]
	\centering
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/t_hadronic.png}
		\caption{}
		\label{fig:t_hadronic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/t_radiative.png}
		\caption{}
		\label{fig:t_radiative}
	\end{subfigure}
	\caption{Mandelstam $|t|$ distribution for all combinations passing analysis cuts for the (a) hadronic and (b) radiative decay. The falloff at low $|t|$ is due to poor experimental acceptance. These results are not acceptance-corrected.}
	\label{fig:t}
\end{figure}

%\section{Multivariate Methods}
%The method presented in this chapter is able to extract a very pure sample of photoproduced $\omega$ mesons, but at small efficiency. 
%Once work on the Monte Carlo has been completed, it will be possible to utilize machine learning techniques to select more combinations while maintaining signal purity.