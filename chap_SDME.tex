\chapter{Measurement of Spin Density Matrix Elements}
\label{chap:SDME}
In this Chapter, we perform a fit of the angular distributions presented in Section \ref{sec:Angular_Distributions} to our experimental data selected in Chapter \ref{ch:eventselection}. 
We also discuss the systematic uncertainties associated with the measurement.

\section{Likelihood Fit}
\label{sec:LikelihoodFit}
In order to extract the SDMEs from the experimental data selected in Chapter \ref{ch:eventselection}, we perform an unbinned maximum likelihood fit using the AmpTools framework \cite{AmpTools}. 
The AmpTools framework is designed to be an experiment-independent implementation of fitting routines for partial wave analyses. 
In this analysis, we are not fitting to the amplitudes, but instead directly fit the intensity given by equation \ref{eq:W_decom} for each of the decay modes.
While perhaps not the designed application, AmpTools still provides a useful set of tools for performing the acceptance correction for the fit, and displaying the results.

AmpTools is designed to fit experimental data in terms of physics amplitudes. 
The intensity as a function of the kinematics is given as
\begin{eqnarray}
I(\Omega)&=&\sum_{\alpha}\left | \sum_{\beta} V_{\alpha\beta}A_{\alpha\beta} (\Omega) \right |^2.
\end{eqnarray}
The physics amplitudes $A_{\alpha\beta}$ are provided by the user for the desired reaction. 
The parameters to be fit are the complex production amplitudes $V_{\alpha\beta}$. 
The intensity is calculated as a coherent sum over $\beta$ and an incoherent sum over $\alpha$.
The parameters required to calculate the physics amplitudes are labeled as $\Omega$.

In our specific case, there is only an incoherent sum over the intensity distribution modeled by $W(\cos\theta,\phi)$.
The AmpTools framework provides the flexibility to fit parameters in the physics amplitudes as well as the $V_{\alpha\beta}$ coefficients. 
Our fit is therefore simply to the intensity
\begin{eqnarray}
I(\bm{\rho},\Phi,\cos\theta,\phi)&=&|V|^2 W(\bm{\rho},\Phi,\phi,\cos\theta)
\end{eqnarray}
where $\bm{\rho}$ are the measurable SDMEs, $\Phi$ is the angle of the production plane relative to the photon polarization direction, and $\cos\theta$ and $\phi$ are the decay angles in the helicity frame as described in Section \ref{sec:Angular_Distributions}.
The parameters to be fit are the SDMEs, $\bm{\rho}$, and the overall normalization, $V$.
In order to relate the modeled intensity to what is actually measured in the experiment, we must additionally include the experimental acceptance $\eta(\Omega)$. 
The number of predicted events including this acceptance function is given by
\begin{equation}
N_{predicted} \equiv \mu = \int I(\Omega) \, \eta(\Omega)\, d\Omega.
\end{equation}
The fit is performed by maximizing the likelihood function
\begin{eqnarray}
\label{eq:likelihood}
\mathcal{L} &=& \frac{e^{-\mu}\mu^{N}}{N!} \prod_{i=1}^{N}\frac{I(\Omega_i)\eta(\Omega_i)}{\mu}
\end{eqnarray}
where $\mu$ is the number of predicted events and $N$ is the number observed.
One can imagine that in the process of minimization, derivatives of equation \ref{eq:likelihood} with respect to the fit parameters could be quite unruly.
Thankfully, it is an equivalent optimization problem to maximize the log likelihood.
After taking the log likelihood, the product becomes a sum over the elements and the problem becomes numerically feasible \cite{RyanAmpTools}.
Ignoring constant terms, we may write 
\begin{align*}
\ln \mathcal{L} &= \sum_{i=1}^N \ln \left( \sum_{\alpha,\beta}^{N_{\textnormal{amps}}} V_{\alpha} V_{\beta}^{*} A_{\alpha}(\Omega)A_{\beta}(\Omega)^{*} \right) - \sum_{\alpha,\beta}^{N_{\textnormal{amps}}} V_{\alpha} V_{\beta}^{*} \int \eta(\Omega) A_{\alpha}(\Omega) A_{\beta}(\Omega)^{*} d\Omega.
\end{align*}
The normalization integral responsible for the acceptance correction can be calculated using Monte Carlo generated uniformly over the volume of phase space.
\begin{align*}
\label{eq:NormInt}
\int \eta(\Omega) A_{\alpha}(\Omega) A_{\beta}(\Omega)^{*} d\Omega &= \mathcal{V} \frac{1}{N_{\textnormal{gen}}} \sum_{i=1}^{N_{\textnormal{gen}}} \eta(\Omega) A_{\alpha}(\Omega) A_{\beta}(\Omega)^{*} 
\end{align*}
where $\mathcal{V}$ is the volume of phase space and can be ignored in the optimization since it is a constant. 
$N_{\textnormal{gen}}$ is the number of generated Monte Carlo events.
The acceptance function $\eta(\Omega)$ is either $0$ or $1$ depending on whether the event was rejected or accepted. 
Thus we may write equation \ref{eq:NormInt} as
\begin{align}
\int \eta(\Omega) A_{\alpha}(\Omega) A_{\beta}(\Omega)^{*} d\Omega &\rightarrow \frac{1}{N_{\textnormal{gen}}} \sum_{i=1}^{N_{\textnormal{Accepted}}} A_{\alpha}(\Omega) A_{\beta}(\Omega)^{*} 
\end{align}
for the purpose of optimization \cite{MattAmpTools}.

There are four data samples that go into the AmpTools fit.
\begin{enumerate}
	\item A Monte Carlo sample generated flat over the measured angles.
	\item This same Monte Carlo sample processed through the detector simulation with the same analysis cuts as the data. These first two samples provide the input to the acceptance correction.
	\item The data sample to be fit.
	\item Additional "background" events. In this analysis, the only background considered are particle combinations arising from accidental photon tags.
\end{enumerate}
The success of the minimization is sensitive to the initial parameters used in the fit. 
The starting values used for each of the parameters can be found in Table \ref{tab:AmpTools_Init}.
These were determined by fitting the data with some parameters fixed.
The results of these fits were then used as the starting points for fits with fewer fixed values until a reasonable stable set of initial parameters was found for the full fit. 
In order to investigate the effect of these starting values on the results after minimization, a study was performed by randomly selecting the initial parameters.
There are few local minima observed, and the results reported are observed to be from the true global minimum of the negative log-likelihood when starting from the set of parameters in Table \ref{tab:AmpTools_Init}. 

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Parameter & Hadronic Decay & Radiative Decay \\
		\hline
		\hline
		$\rho_{00}^{0}$ & 0.07 & 0.07\\ \hline
		$\rho_{10}^{0}$ & 0.07 & 0.07\\ \hline
		$\rho_{1-1}^{0}$ & -0.07 & -0.07\\ \hline
		$\rho_{11}^{1}$ & -0.10 & -0.10\\ \hline
		$\rho_{00}^{1}$ & 0.00 & 0.00 (fixed)\\ \hline
		$\rho_{10}^{1}$ & -0.10 & -0.10\\ \hline
		$\rho_{1-1}^{1}$ & 0.20 & 0.35\\ \hline
		$\rho_{10}^{2}$ & 0.00 & 0.10\\ \hline
		$\rho_{1-1}^{2}$ & -0.20 & -0.35\\ \hline
	\end{tabular}
	\caption{Initial values used in AmpTools fit.}
	\label{tab:AmpTools_Init}
\end{table}

The nominal polarization directions are $\phi_{\parallel} = 0^{\circ}$ and $\phi_{\perp} = 90^{\circ}$ relative to the lab coordinates. However, a small offset exists in the data. Fitting a histogram of the accidentals-subtracted $\phi$ distribution of the decay plane in the $\omega$ helicity frame with a function of the form $f(\phi)=p_0+p_1\cos(2(\phi-\phi_0))$ 
yields ${\phi_0} = 3.59 \pm 0.51^{\circ}$ for PARA polarization and ${\phi_0} = 2.39 \pm 0.46^{\circ}$ for PERP polarization. 
Alternatively, this angle can be allowed to float in the AmpTools fit. 
Using this second method yields ${\phi_0} = 3.08 \pm 0.20^{\circ}$ for PARA polarization and ${\phi_0} = 3.35 \pm 0.24^{\circ}$ for PERP polarization. 
The two methods yield consistent results. 
The latter values are used in the fits that follow and are fixed in the minimization.

\section{Monte Carlo Sample for Acceptance Correction}
The generator used for our acceptance correction to the measured data was implemented using AmpTools.
The intensity is the product of a Breit-Wigner describing the resonance, an exponential t-dependence describing the diffractive nature of the reaction, and the available phase space to the particular kinematics.
The t-slope is taken to be $3.5 \textnormal{ GeV}^{-2}$ to enhance the fraction of simulated events entering the geometrical experimental acceptance.
This is smaller than previous measured values \cite{Ballam73}, but does not affect our fit results for the SDMEs.
These thrown events are then processed with a Hall-D specific Geant3 based simulation tool called \verb|HDGeant|. 
The data are then smeared according to instrumental resolutions in a program called \verb|mcsmear|.
These smeared data are then subjected to the same full set of analysis cuts as the real data.
The number of thrown events, and the number passing all selection cuts are shown in Table \ref{tab:ThrownAccMC}.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		Decay & MC Events Thrown & MC Events Accepted & Percent Accepted \\
		\hline
		$\omega \rightarrow \threebody$ & 7,880,000 & 245,543 & $3.1 \%$ \\
		\hline
		$\omega \rightarrow \twobody$ & 9,880,000 & 98,680 &  $1.0 \%$ \\
		\hline
	\end{tabular}
	\caption{Number of thrown and accepted Monte Carlo events used for acceptance corrections. }
	\label{tab:ThrownAccMC}
\end{table}

\section{Fit Results} \label{sec:fit_results}
The fit results for the hadronic decay in bins of $|t|$ are shown in Figure \ref{fig:had_SDME_statOnly}. The fit results for the radiative decay in a single bin of $|t|$ are shown in figure \ref{fig:rad_SDME_statOnly}. 
In order to investigate the quality of the fit, we may plot the accidentals-subtracted data against our accepted Monte Carlo that has been re-weighted by the result of the fit. 
A comparison in the lowest t-bin in the hadronic PARA data set is shown in Figure \ref{fig:Fit_hadronic}. 
The results match quite well in each of the angular variables fit for the decay distribution.
Due to relative sample sizes and incomplete understanding of the systematic effects, the radiative sample is more challenging to fit.
A comparison in the lowest t-bin in the radiative PARA data set is shown in Figure \ref{fig:Fit_radiative}. 
In the radiative sample, there is a strong acceptance effect in $\cos\theta$ at forward and backward angles, and in $\phi$ near zero and $\pi$ attributed to the excluded calorimeter regions.
These appear to be modeled fairly well, but additional investigation of the quality of the acceptance correction may be needed in the future.

In the fits for the radiative decay, the $\rho^{1}$ elements show strong correlations leading to large statistical errors in the fit parameters. 
The problem is mitigated by fixing one of the parameters. 
In order to investigate the effect this has on the final parameters, the value is varied by $\pm2\sigma$ of the measurement in the hadronic channel and included in the systematic uncertainties determined in Section \ref{sec:systematic_uncertainties}.
$\rho_{00}^{1}$ was chosen since the hadronic measurements for this parameter are uniformly consistent with zero across the measured $t$ range. 

\begin{figure}[b!]
	\includegraphics[width=\hsize]{figures/hadronicVst.pdf}
	\caption{Fit results for the measurable SDMEs in the hadronic decay channel for the two separate polarization orientations. Error bars indicate only statistical errors returned by AmpTools.}
	\label{fig:had_SDME_statOnly}
\end{figure} 

\begin{figure}[tb!]
	\includegraphics[width=\hsize]{figures/radiativeVst.pdf}
	\caption{Fit results for the measurable SDMEs in the radiative decay channel for the two separate polarization orientations. SDME $\rho_{00}^{1}$ is fixed in the fit. Error bars indicate only statistical errors returned by AmpTools. Parameter $\rho_{00}^{1}$ is fixed in the minimization.}
	\label{fig:rad_SDME_statOnly}
\end{figure}

\begin{figure}[tbh]
	\centering
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/hadronic/Bin0Phi.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/hadronic/Bin0CosTheta.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/hadronic/Bin0phi-1.pdf}
	\end{subfigure}%
	\caption{Comparison of data with the result of the fit for $|t| \in [0.100, 0.350)$, \hadronic decay, PARA polarization. The points are the experimental data, the red filled histogram is the contribution from the tagger-accidental background, and the green filled histogram is the re-weighted accepted Monte Carlo. The angle $\Phi$ is the angle of the decay plane relative to the photon polarization vector, and $\cos\theta$ and $\phi$ are the decay angles in the helicity frame as described in Section \ref{sec:Angular_Distributions}.}
	\label{fig:Fit_hadronic}
\end{figure}

%\begin{figure}
%	\includegraphics[width=\hsize]{figures/FitResultComparePARA.png}
%	\caption{Comparison of accidental-subtracted data with accepted Monte Carlo re-weighted by the results of the fit. The points are the experimental data, and the filled histogram is the re-weighted Monte Carlo.}
%	\label{fig:fit_vs_mc}
%\end{figure}

\begin{figure}[tbh]
	\centering
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/radiative/Bin0Phi.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/radiative/Bin0CosTheta.pdf}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.33\hsize}
		\centering
		\includegraphics[width=\textwidth]{figures/ReweightedFits/radiative/Bin0phi-1.pdf}
	\end{subfigure}%
	\caption{Comparison of data with the result of the fit for $|t| \in [0.100, 0.600)$, \radiative decay, PARA polarization. The points are the experimental data, the red filled histogram is the contribution from the tagger-accidental background, and the green filled histogram is the re-weighted accepted Monte Carlo. The angle $\Phi$ is the angle of the decay plane relative to the photon polarization vector, and $\cos\theta$ and $\phi$ are the decay angles in the helicity frame as described in Section \ref{sec:Angular_Distributions}.}
	\label{fig:Fit_radiative}
\end{figure}
\FloatBarrier
\section{Systematics Studies}
\label{sec:systematic_uncertainties}
According to the fits in Section \ref{sec:fit_results} there is a discrepancy in the SDMEs extracted for the two perpendicular polarization states. 
These SDME measurements represent the first physics results to come from the GlueX experiment that require the experimental acceptance to be correctly modeled.
At the time of writing this thesis, studies are still ongoing to appropriately match Monte Carlo detector resolutions and efficiencies with data. 
The SDMEs presented here use an acceptance correction based on the current state of the Monte Carlo that has had minimal tuning to match the data.
It is a testament to the quality of the simulation that the results achieved are even nearly consistent with minimal tuning.
In this section, we will attempt to quantify the effect of our selection cuts on the measured SDMEs.
We will then discuss other remaining systematics that may affect the measurement.

The exact placement of the selection cuts presented in Table \ref{tab:cuts} is somewhat arbitrary.
In order to estimate the systematic uncertainty caused by this selection, each of the cut parameters is varied around the selected value. 
These samples with the adjusted cut are then fit for the SDMEs. 
The standard deviation of the measurements in this ensemble are taken to be the systematic uncertainty associated with this selection. 
The t-bin size has been widened for these studies to minimize the statistical error that may impact this measurement.

Consider the following example. 
One of the analysis cuts is on the minimum distance between a reconstructed photon and the beamline in the FCAL. 
The nominal cut value is $\gamma_{R} > 20.0 \textnormal{ cm}$. 
We can vary this cut in steps of $1 \textnormal{ cm}$ from $15$ cm to $25$ cm. 
The data and Monte Carlo samples are then reprocessed with the new cut value before a new fit is performed.
The variation of the measured SDMEs with respect to this cut parameter is shown for the two decay modes in Figures \ref{fig:variedFCALR_hadronic} and \ref{fig:variedFCALR_radiative}.
In the hadronic decay, there is little effect on the measured values of the SDME by changing this cut, but in the radiative decay there is a very strong dependence. 
The exact cause of this dependence is unknown. 
Using the prescription above, we assign the following values to the systematic uncertainty arising from this cut in the two decay modes.
The values are listed in Table \ref{tab:single_varied_parameter}.
This process is then repeated for all of the cuts placed on the data. 
The total systematic error is determined by adding all of the individual components in quadrature.
This assumes the individual contributions are uncorrelated.
It is also assumed that the systematic uncertainties do not vary with $t$. 
In Table \ref{tab:all_varied_parameter} we present the total systematic uncertainties assigned to each of the SDMEs based on varying our input parameters to the analysis.
A full table of the individual components of the systematic uncertainties can be found in Appendix \ref{app:SystematicUncertainties}.

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\hsize]{figures/VariedParameter_dMinFCALR_hadronic.pdf}
	\caption{Measured SDMEs for variations of the minimum photon FCAL radius cut for the hadronic decay $\omega \rightarrow \pi^{+} \pi^{-}\pi^{0}$ in PARA and PERP polarization orientations.}
	\label{fig:variedFCALR_hadronic}
\end{figure} 

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\hsize]{figures/VariedParameter_dMinFCALR_radiative.pdf}
	\caption{Measured SDMEs for variations of the minimum photon FCAL radius cut for the radiative decay $\omega \rightarrow \pi^{0} \gamma$ in PARA and PERP polarization orientations.}
	\label{fig:variedFCALR_radiative}
\end{figure} 

\begin{table}[!htbp]

	\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c| }
		\hline
		Dataset & $\sigma \rho_{00}^{0}$ &  $\sigma \rho_{10}^{0}$  & $\sigma \rho_{1-1}^{0}$  & $\sigma \rho_{11}^{1}$  & $\sigma \rho_{00}^{1}$  & $\sigma \rho_{10}^{1}$  & $\sigma \rho_{1-1}^{1}$  & $\sigma \rho_{10}^{2}$ & $\sigma \rho_{1-1}^{2}$  \\
		\hline
		Hadronic Decay PARA & 0.002 &      0.003 &      0.003 &      0.001 &      0.001 &      0.001 &      0.002 &      0.003 &      0.003 \\
		Hadronic Decay PERP &  0.001 &      0.003 &      0.003 &      0.002 &      0.001 &      0.001 &      0.002 &      0.001 &      0.002 \\
		Radiative Decay PARA &  0.022 &      0.033 &      0.023 &      0.013 &      fixed &      0.016 &      0.016 &      0.013 &      0.016 \\
		Radiative Decay PERP & 0.019 &      0.033 &      0.024 &      0.017 &      fixed &      0.028 &      0.017 &      0.021 &      0.010\\
		\hline
	\end{tabular}
	\caption{Systematic uncertainties assigned to the SDMEs for variations of minimum FCAL R cut.}
	\label{tab:single_varied_parameter}
\end{table}

\begin{table}[!htbp]
	
	\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c| }
		\hline
		Dataset & $\sigma \rho_{00}^{0}$ &  $\sigma \rho_{10}^{0}$  & $\sigma \rho_{1-1}^{0}$  & $\sigma \rho_{11}^{1}$  & $\sigma \rho_{00}^{1}$  & $\sigma \rho_{10}^{1}$  & $\sigma \rho_{1-1}^{1}$  & $\sigma \rho_{10}^{2}$ & $\sigma \rho_{1-1}^{2}$  \\
		\hline
		Hadronic Decay PARA & 0.018 &      0.021 &      0.030 &      0.018 &      0.014 &      0.014 &      0.041 &      0.010 &      0.031 \\
		Hadronic Decay PERP &  0.018 &      0.020 &      0.032 &      0.019 &      0.009 &      0.019 &      0.035 &      0.012 &      0.031 \\
		Radiative Decay PARA &  0.090 &     0.073 &      0.077 &      0.044 &      fixed &      0.055 &      0.057 &      0.046 &      0.049 \\
		Radiative Decay PERP & 0.089 &      0.075 &      0.076 &      0.047 &      fixed &      0.072 &      0.056 &      0.045 &      0.068 \\
		\hline
	\end{tabular}
	\caption{Systematic uncertainties assigned to the SDMEs based on parameter variation.}
	\label{tab:all_varied_parameter}
\end{table}

\begin{figure}[b!]
	\includegraphics[width=\hsize]{figures/hadronicVstWithSystematics.pdf}
	\caption{Fit results for the measurable SDMEs in the \hadronic decay channel for the two separate polarization orientations. Error bars indicate the quadrature addition of statistical errors returned by AmpTools and the systematic uncertainties assigned in Table \ref{tab:all_varied_parameter}.}
	\label{fig:had_SDME_statPlusSys}
\end{figure}

\begin{figure}[tb]
	\includegraphics[width=\hsize]{figures/radiativeVstWithSystematics.pdf}
	\caption{Fit results for the measurable SDMEs in the \radiative decay channel for the two separate polarization orientations. Error bars indicate the quadrature addition of statistical errors returned by AmpTools and the systematic uncertainties assigned in Table \ref{tab:all_varied_parameter}. Parameter $\rho_{00}^{1}$ is fixed in the minimization.}
	\label{fig:rad_SDME_statPlusSys}
\end{figure}

The fit results for the SDMEs including the systematic errors assigned in Table \ref{tab:all_varied_parameter} are found in Figures \ref{fig:had_SDME_statPlusSys} and \ref{fig:rad_SDME_statPlusSys}. 
After including the systematic effects our selection criteria have on our measurements, the PARA and PERP datasets mostly agree to within errors.
We assign one additional systematic uncertainty in combining the results of the two datasets. 
Our final result is taken to be the average of the SDMEs in each t-bin.
Half of the average discrepancy between PARA and PERP polarization fits in each bin is taken as the additional systematic and is added in quadrature to the average systematic uncertainties from Table \ref{tab:all_varied_parameter}.
The systematic error assigned to each configuration for the PARA/PERP discrepancy is presented in Table \ref{tab:para_perp_discrep}. 
The final results including the total statistical and reported systematic uncertainty are collected in Table \ref{tab:results_final}.
Comparisons of these results and the OTL model presented in Section \ref{sec:OhTitovLee} for both decay modes are shown in Figures \ref{fig:had_SDME_final} and \ref{fig:rad_SDME_final}.

\begin{table}
	\centering
	\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c| }
		\hline
		Dataset & $\sigma \rho_{00}^{0}$ &  $\sigma \rho_{10}^{0}$  & $\sigma \rho_{1-1}^{0}$  & $\sigma \rho_{11}^{1}$  & $\sigma \rho_{00}^{1}$  & $\sigma \rho_{10}^{1}$  & $\sigma \rho_{1-1}^{1}$  & $\sigma \rho_{10}^{2}$ & $\sigma \rho_{1-1}^{2}$  \\
		\hline
		%Hadronic Decay & 0.003 & 0.001 & 0.005 & 0.009 & 0.008 & 0.008 & 0.039 & 0.011 & 0.028 \\
		Hadronic Decay & 0.003 & 0.001 & 0.005 & 0.014 & 0.008 & 0.004 & 0.021 & 0.008 & 0.016 \\
		%Radiative Decay &  0.021 & 0.006 & 0.005 & 0.004 & 0.000 & 0.018 & 0.072 & 0.065 & 0.024 \\
		Radiative Decay &  0.021 & 0.004 & 0.005 & 0.004 & fixed & 0.020 & 0.058 & 0.065 & 0.005 \\
		\hline
	\end{tabular}
	\caption{Systematic uncertainties assigned to the SDMEs to address the PARA/PERP discrepancy.}
	\label{tab:para_perp_discrep}
\end{table}

\begin{table}
		\centering
		\begin{tabular}{ |c|c|c|c|c|c|c|c|c|c| }
		\hline
		Dataset & $\sigma \rho_{00}^{0}$ &  $\sigma \rho_{10}^{0}$  & $\sigma \rho_{1-1}^{0}$  & $\sigma \rho_{11}^{1}$  & $\sigma \rho_{00}^{1}$  & $\sigma \rho_{10}^{1}$  & $\sigma \rho_{1-1}^{1}$  & $\sigma \rho_{10}^{2}$ & $\sigma \rho_{1-1}^{2}$  \\
		\hline
		%Hadronic Decay & 0.017 & 0.020 & 0.030 & 0.020 & 0.014 & 0.017 & 0.050 & 0.015 & 0.036 \\
		Hadronic Decay & 0.018 & 0.021 & 0.031 & 0.023 & 0.014 & 0.017 & 0.043 & 0.014 & 0.035 \\
		%Radiative Decay & 0.092 & 0.074 & 0.077 & 0.032 & 0.000 & 0.065 & 0.089 & 0.079 & 0.059 \\
		Radiative Decay & 0.092 & 0.074 & 0.077 & 0.046 & 0.000 & 0.067 & 0.081 & 0.079 & 0.059 \\
		\hline
		\end{tabular}
	\caption{Total systematic uncertainties assigned to the SDMEs described in Section \ref{sec:systematic_uncertainties}.}
	\label{tab:total_systematic_uncertainties}
\end{table}

\begin{table}
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		Decay Mode & \multicolumn{4}{c|}{$\omega\rightarrow\pi^{+}\pi^{-}\pi^{0}$} & $\omega\rightarrow\pi^{0}\gamma$ \\
		\hline
		|t|-bin [$\textnormal{GeV}^2$]& $0.100-0.175 $ & $0.175-0.250$ & $0.250-0.350$ & $0.350-0.800$  & $0.100-0.600$ \\
		\specialrule{.1em}{.05em}{.05em}
		$\rho_{00}^{0}$ & $0.072 \pm 0.019$ &$0.073 \pm 0.019$ &$0.077 \pm 0.019$ &$0.102 \pm 0.019$ & $0.096 \pm 0.104$\\
		$\rho_{10}^{0}$ &$0.061 \pm 0.021$ &$0.062 \pm 0.021$ &$0.064 \pm 0.021$ &$0.069 \pm 0.021$ & $0.038 \pm 0.080$ \\
		$\rho_{1-1}^{0}$ & $-0.042 \pm 0.032$ &$-0.076 \pm 0.032$ &$-0.083 \pm 0.032$ &$-0.090 \pm 0.032$ & $-0.053 \pm 0.079$ \\
		$\rho_{11}^{1}$ & $-0.080 \pm 0.029$ &$-0.114 \pm 0.030$ &$-0.115 \pm 0.030$ &$-0.159 \pm 0.031$ & $-0.116 \pm 0.062$ \\ 
		$\rho_{00}^{1}$ & $0.001 \pm 0.021$ &$0.006 \pm 0.022$ &$0.006 \pm 0.023$ &$-0.001 \pm 0.026$ & fixed at 0.0\\
		$\rho_{10}^{1}$ & $-0.094 \pm 0.021$ &$-0.100 \pm 0.022$ &$-0.082 \pm 0.023$ &$-0.074 \pm 0.023$ & $-0.092 \pm 0.108$ \\ 
		$\rho_{1-1}^{1}$ & $0.361 \pm 0.049$ &$0.375 \pm 0.049$ &$0.393 \pm 0.050$ &$0.343 \pm 0.050$ & $0.399 \pm 0.103$ \\
		$\rho_{10}^{2}$ & $0.084 \pm 0.018$ &$0.078 \pm 0.019$ &$0.080 \pm 0.020$ &$0.060 \pm 0.020$ & $0.127 \pm 0.097$ \\ 
		$\rho_{1-1}^{2}$ & $-0.347 \pm 0.040$ &$-0.396 \pm 0.040$ &$-0.369 \pm 0.042$ &$-0.334 \pm 0.042$ & $-0.414 \pm 0.076$ \\
		\hline
	\end{tabular}
	\caption{Combined results for PARA and PERP polarization datasets. The error is the combined statistical and systematic error.}
	\label{tab:results_final}
	
\end{table}

\begin{figure}[tb]
	\includegraphics[width=\hsize]{figures/hadronicVstWithSLACAndOTL.pdf}
	\caption{Fit results for the measurable SDMEs in the hadronic decay channel. Error bars indicate the quadrature addition of statistical errors returned by AmpTools and the systematic uncertainties assigned in Table \ref{tab:total_systematic_uncertainties}. This result is compared with measurements made at SLAC \cite{Ballam73} and predictions from the Oh, Titov, Lee model \cite{OhTitovLee}.}
	\label{fig:had_SDME_final}
\end{figure}

\begin{figure}[tb]
	\includegraphics[width=\hsize]{figures/radiativeVstWithOTL.pdf}
	\caption{Fit results for the measurable SDMEs in the radiative decay channel. Error bars indicate the quadrature addition of statistical errors returned by AmpTools and the systematic uncertainties assigned in Table \ref{tab:total_systematic_uncertainties}. Parameter $\rho_{00}^{1}$ is fixed in the minimization. This result is compared with predictions from the Oh, Titov, Lee model \cite{OhTitovLee}.}
	\label{fig:rad_SDME_final}
\end{figure}

\section{Discussion of Results}
\label{sec:discussion_of_results}

In this chapter we have presented preliminary measurements of the SDMEs in $\omega$ photoproduction at $E_{\gamma} = 8.4-9.0$ GeV with the GlueX detector.
Results are presented in two decay modes and are in agreement according to our current understanding of the experimental uncertainties.
These results are generally consistent with a simple model based on Pomeron and pseudoscalar t-channel exchange mechanisms.
However, there is some significant deviation from the model in some of the elements. 
It will be important to investigate if this result holds up to continued scrutiny. 
Deviations from the OTL model in $\rho_{1-1}^{1}$ and Im$\rho_{1-1}^{2}$ could be indicative of higher relative Pomeron contribution to the production.
The effect this has on the predicted SDMEs is shown in Figure \ref{fig:had_SDME_VaryPomeronContribution}.
The Pomeron contribution must be enhanced by roughly 20\% to match the behavior observed in the data in the two most significant non-zero elements, $\rho_{1-1}^1$ and Im$\rho_{1-1}^2$.
The results are also consistent with earlier measurements at $E_{\gamma}=9.3 \textnormal{ GeV}$ at SLAC \cite{Ballam73}, though the earlier measurements suffer from limited statistics.

\begin{figure}[tb]
	\includegraphics[width=\hsize]{figures/hadronicVstWithSLACAndOTL_PomeronStrength.pdf}
	\caption{Fit results for the measurable SDMEs in the hadronic decay channel showing the effect of varying the Pomeron contribution to the OTL amplitudes.}
	\label{fig:had_SDME_VaryPomeronContribution}
\end{figure}
\FloatBarrier
These results represent the most precise measurement of the SDMEs in $\omega$ photoproduction at $E_{\gamma}=9 \textnormal{ GeV}$ to-date\footnote{Please note that the results presented in this thesis are considered preliminary until thoroughly vetted by the entire GlueX collaboration.}.
Even though this represents a two order-of-magnitude increase over the world data in statistics at these energies, the data used in this analysis from Spring 2016 constitutes less than $10 \%$ of the total physics data recorded so far by the GlueX detector, and there are several years of additional running planned.
This increased data will allow extending this study to higher t-values where the interplay of the amplitudes leads to more interesting behavior of the SDMEs.
Of course, these measurements will continue to improve as our systematic uncertainties are better understood.
%The measurements provide a clear indication of where additional efforts are needed in refining our understanding of the reconstruction.
Even with our current understanding of the results, these measurements can be used to improve existing models of $\omega$ photoproduction.
It is clear from this analysis that the rich structure of the experimental data at GlueX and its unique detection capabilities will continue to provide ample opportunities for great advancement in our understanding of QCD processes in photoproduction for years to come.

